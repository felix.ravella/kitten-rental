export type AmplifyDependentResourcesAttributes = {
    "api": {
        "kittenrental": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}