import { createRouter, createWebHashHistory } from 'vue-router'
import KittenView from '../views/KittenView.vue'

const routes = [
  {
    path: '/',
    name: 'kittens',
    component: KittenView
  },
  {
    path: '/kittens',
    name: 'kittens',
    component: KittenView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
