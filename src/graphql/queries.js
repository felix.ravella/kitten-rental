/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getKitten = /* GraphQL */ `
  query GetKitten($id: ID!) {
    getKitten(id: $id) {
      id
      name
      gender
      color
      age
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listKittens = /* GraphQL */ `
  query ListKittens(
    $filter: ModelKittenFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listKittens(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      name
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getRental = /* GraphQL */ `
  query GetRental($id: ID!) {
    getRental(id: $id) {
      id
      start_timestamp
      end_timestamp
      kitten {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      user {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
      kittenRentalsId
      userRentalsId
    }
  }
`;
export const listRentals = /* GraphQL */ `
  query ListRentals(
    $filter: ModelRentalFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listRentals(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        start_timestamp
        end_timestamp
        kitten {
          id
          name
          gender
          color
          age
          createdAt
          updatedAt
        }
        user {
          id
          name
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
        kittenRentalsId
        userRentalsId
      }
      nextToken
    }
  }
`;
