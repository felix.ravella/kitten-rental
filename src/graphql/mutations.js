/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createKitten = /* GraphQL */ `
  mutation CreateKitten(
    $input: CreateKittenInput!
    $condition: ModelKittenConditionInput
  ) {
    createKitten(input: $input, condition: $condition) {
      id
      name
      gender
      color
      age
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateKitten = /* GraphQL */ `
  mutation UpdateKitten(
    $input: UpdateKittenInput!
    $condition: ModelKittenConditionInput
  ) {
    updateKitten(input: $input, condition: $condition) {
      id
      name
      gender
      color
      age
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteKitten = /* GraphQL */ `
  mutation DeleteKitten(
    $input: DeleteKittenInput!
    $condition: ModelKittenConditionInput
  ) {
    deleteKitten(input: $input, condition: $condition) {
      id
      name
      gender
      color
      age
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      name
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      name
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      name
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createRental = /* GraphQL */ `
  mutation CreateRental(
    $input: CreateRentalInput!
    $condition: ModelRentalConditionInput
  ) {
    createRental(input: $input, condition: $condition) {
      id
      start_timestamp
      end_timestamp
      kitten {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      user {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
      kittenRentalsId
      userRentalsId
    }
  }
`;
export const updateRental = /* GraphQL */ `
  mutation UpdateRental(
    $input: UpdateRentalInput!
    $condition: ModelRentalConditionInput
  ) {
    updateRental(input: $input, condition: $condition) {
      id
      start_timestamp
      end_timestamp
      kitten {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      user {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
      kittenRentalsId
      userRentalsId
    }
  }
`;
export const deleteRental = /* GraphQL */ `
  mutation DeleteRental(
    $input: DeleteRentalInput!
    $condition: ModelRentalConditionInput
  ) {
    deleteRental(input: $input, condition: $condition) {
      id
      start_timestamp
      end_timestamp
      kitten {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      user {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
      kittenRentalsId
      userRentalsId
    }
  }
`;
