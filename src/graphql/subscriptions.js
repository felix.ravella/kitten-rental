/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateKitten = /* GraphQL */ `
  subscription OnCreateKitten {
    onCreateKitten {
      id
      name
      gender
      color
      age
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateKitten = /* GraphQL */ `
  subscription OnUpdateKitten {
    onUpdateKitten {
      id
      name
      gender
      color
      age
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteKitten = /* GraphQL */ `
  subscription OnDeleteKitten {
    onDeleteKitten {
      id
      name
      gender
      color
      age
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser {
    onCreateUser {
      id
      name
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser {
    onUpdateUser {
      id
      name
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser {
    onDeleteUser {
      id
      name
      rentals {
        items {
          id
          start_timestamp
          end_timestamp
          createdAt
          updatedAt
          kittenRentalsId
          userRentalsId
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateRental = /* GraphQL */ `
  subscription OnCreateRental {
    onCreateRental {
      id
      start_timestamp
      end_timestamp
      kitten {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      user {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
      kittenRentalsId
      userRentalsId
    }
  }
`;
export const onUpdateRental = /* GraphQL */ `
  subscription OnUpdateRental {
    onUpdateRental {
      id
      start_timestamp
      end_timestamp
      kitten {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      user {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
      kittenRentalsId
      userRentalsId
    }
  }
`;
export const onDeleteRental = /* GraphQL */ `
  subscription OnDeleteRental {
    onDeleteRental {
      id
      start_timestamp
      end_timestamp
      kitten {
        id
        name
        gender
        color
        age
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      user {
        id
        name
        rentals {
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
      kittenRentalsId
      userRentalsId
    }
  }
`;
